#!/bin/bash
set -e

# Update Matomo database in case there is a new version.
if [ -f /var/www/html/config/config.ini.php ];
then php console core:update --yes;
php console core:create-security-files;
fi
