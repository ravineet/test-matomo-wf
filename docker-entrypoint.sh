#!/bin/sh
set -e

pwd

sed "s/APPLICATION_NAME/${APPLICATION_NAME}/g" -i  /etc/apache2/conf-available/httpdcern10.conf
#sed "s/APPLICATION_NAME/${APPLICATION_NAME}/g" -i  /usr/src/matomo/config/config.ini.php
#sed "s/MYSQL_SERVER/${MYSQL_SERVER}/g" -i  /usr/src/matomo/config/config.ini.php
#sed "s/MYSQL_USERNAME/${MYSQL_USERNAME}/g" -i  /usr/src/matomo/config/config.ini.php
#sed "s/MYSQL_PASSWORD/${MYSQL_PASSWORD}/g" -i  /usr/src/matomo/config/config.ini.php
#sed "s/MYSQL_DATABASE/${MYSQL_DATABASE}/g" -i  /usr/src/matomo/config/config.ini.php
#sed "s/MYSQL_PORT/${MYSQL_PORT}/g" -i  /usr/src/matomo/config/config.ini.php

#replace LoginOIDC defaults wth CERN ones
sed -i "s|OAuth login|CERN OAuth login|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
sed -i "s|https://github.com/login/oauth/authorize|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
sed -i "s|https://github.com/login/oauth/access_token|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
sed -i "s|https://api.github.com/user|https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo|" /var/www/html/plugins/LoginOIDC/SystemSettings.php
sed -i "s|i\"id\"|\"sub\"|" /var/www/html/plugins/LoginOIDC/SystemSettings.php

#Config files from an Upgrade. Without this there are filesystem check errors
cp -pv /usr/src/matomo/config/manifest.inc.php /var/www/html/config/manifest.inc.php  && \
cp -pv /usr/src/matomo/config/global.php /var/www/html/config/global.php  && \
cp -pv /usr/src/matomo/config/global.ini.php /var/www/html/config/global.ini.php  && \
cp -prv /usr/src/matomo/config/environment /var/www/html/config/


#Post installation
echo "Post installation: Commands to run after the initial setup and the Database has been configured "
echo "cd /var/www/html"
echo "php console config:set 'mail.transport=\"smtp\"' 'mail.port=\"25\"' 'mail.host=\"cernmx.cern.ch\"' 'mail.encryption=\"tls\"'"
echo "php console config:set 'General.force_ssl=1'"
echo "php console config:set 'log.log_writers[]=\"file\"' 'log.log_level=\"DEBUG\"'"

#Active the plugins
echo "php console plugin:activate LogViewer"
echo "php console plugin:activate LoginOIDC"
echo "php console plugin:activate EnvironmentVariables"

#Configure archiving
echo "Enable archiving through cronjob via the General settings"

echo "Show variables"
echo ${MATOMO_DATABASE_HOST}
echo $MATOMO_DATABASE_USERNAME
echo $MATOMO_DATABASE_DBNAME

echo "Start apache in the foreground"
apache2-foreground
